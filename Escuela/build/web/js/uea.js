//ruta de nuestro servicio
var rootURL = "http://localhost:8080/Escuela/webresources/uea";

var currentUea; //estas es una variable que va guardar los datos parseados del formulario a JSON
var bander = 0;
var regisAlu;
//imprimimos la lista 
findAll();

//ocutamos el boton de borrado
$('#btnDelete').hide();

// Register listeners
$('#btnSearch').click(function() {
	search($('#searchKey').val());
	return false;
});

// Trigger search when pressing 'Return' on search key input field
$('#searchKey').keypress(function(e){
	if(e.which == 13) {
		search($('#searchKey').val());
		e.preventDefault();
		return false;
    }
});

$('#btnAdd').click(function() {
	newUea();
        //creamos una bandera para detectar si se va registrar un alumno
        bander = 1;
        regisAlu = newbandera(bander);
	return false;
});

$('#btnSave').click(function() {
	if (regisAlu === 1){
		addUea();
                bander = 0;
                regisAlu = newbandera(bander);
            }
	else{
		updateUea();}
	return false;
});

$('#btnDelete').click(function() {
	deleteUea();
	return false;
});

$('#wineList a').live('click', function() {
	findById($(this).data('identity'));
});

// Replace broken images with generic wine bottle
/*$("img").error(function(){
  $(this).attr("src", "pics/generic.jpg");

});*/

function search(searchKey) {
	if (searchKey == '') 
		findAll();
	else
		findByName(searchKey);
}

function newUea() {
	$('#btnDelete').hide();
	currentUea = {};
	renderDetails(currentUea); // Display empty form
}

function newbandera(bandera) {
        var band = bandera
	//activamos bandera
        if( band === 1 ){
            return 1;
        }
        else{
            return 0;
        }
}

function findAll() {
	console.log('imprime toda la lista');
	$.ajax({
		type: 'GET',
		url: rootURL,
		dataType: "json", // data type of response
		success: renderList
	});
}

function findByName(searchKey) {
	console.log('Encontrado por nombre de uea: ' + searchKey);
	$.ajax({
		type: 'GET',
		url: rootURL + '/search/' + searchKey,
		dataType: "json",
		success: renderList 
	});
}

function findById(id) {
	console.log('Encontrado por clave uea: ' + id);
	$.ajax({
		type: 'GET',
		url: rootURL + '/' + id,
		dataType: "json",
		success: function(data){
			$('#btnDelete').show();
			console.log('Encontrado por clave uea exitosamente: ' + data.nombre_uea);
			currentUea = data;
			renderDetails(currentUea);
		}
	});
}

function addUea() {
	console.log('Agregamos uea');
	$.ajax({
		type: 'POST',
		contentType: 'application/json',
		url: rootURL,
		dataType: "json",
		data: formToJSON(),
		success: function(data, textStatus, jqXHR){
			alert('uea creado exitosamente');
			$('#btnDelete').show();
			$('#clave_uea').val(data.clave_uea);
		},
		error: function(jqXHR, textStatus, errorThrown){
			alert('Error al agregar uea: ' + textStatus);
		}
	});
}

function updateUea() {
	console.log('Actualizando uea');
	$.ajax({
		type: 'PUT',
		contentType: 'application/json',
		url: rootURL + '/' + $('#clave_uea').val(),
		dataType: "json",
		data: formToJSON(),
		success: function(data, textStatus, jqXHR){
			alert('uea actualizado exitosamente');
		},
		error: function(jqXHR, textStatus, errorThrown){
			alert('Error hay actualizar uea: ' + textStatus);
		}
	});
}

function deleteUea() {
	console.log('Borrar uea');
	$.ajax({
		type: 'DELETE',
		url: rootURL + '/' + $('#clave_uea').val(),
		success: function(data, textStatus, jqXHR){
			alert('Borrando uea exitosamente');
		},
		error: function(jqXHR, textStatus, errorThrown){
			alert('Error al borrar uea');
		}
	});
}

function renderList(data) {
	// JAX-RS serializes an empty list as null, and a 'collection of one' as an object (not an 'array of one')
	var list = data == null ? [] : (data instanceof Array ? data : [data]);

	$('#wineList li').remove();
	$.each(list, function(index, ueas) {
		$('#wineList').append('<li><a href="#" data-identity="' + ueas.clave_uea + '">'+ ueas.nombre_uea +'</a></li>');
	});
}

function renderDetails(ueas) {
	$('#clave_uea').val(ueas.clave_uea);
	$('#nombre_uea').val(ueas.nombre_uea);
	$('#creditos').val(ueas.creditos);
	//$('#pic').attr('src', 'pics/' + wine.picture); //imagen
}

// Helper function to serialize all the form fields into a JSON string
function formToJSON() {
	return JSON.stringify({
		"clave_uea": $('#clave_uea').val(),
		"nombre_uea": $('#nombre_uea').val(), 
		"creditos": $('#creditos').val()
		});
}




