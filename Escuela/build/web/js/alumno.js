//ruta de nuestro servicio
var rootURL = "http://localhost:8080/Escuela/webresources/alumno";

var currentAlumno; //estas es una variable que va guardar los datos parseados del formulario a JSON
var bander = 0;
var regisAlu;
//imprimimos la lista 
findAll();

//ocutamos el boton de borrado
$('#btnDelete').hide();

// Register listeners
$('#btnSearch').click(function() {
	search($('#searchKey').val());
	return false;
});

// Trigger search when pressing 'Return' on search key input field
$('#searchKey').keypress(function(e){
	if(e.which == 13) {
		search($('#searchKey').val());
		e.preventDefault();
		return false;
    }
});

$('#btnAdd').click(function() {
	newAlumno();
        //creamos una bandera para detectar si se va registrar un alumno
        bander = 1;
        regisAlu = newbandera(bander);
	return false;
});

$('#btnSave').click(function() {
	if (regisAlu === 1){
		addAlumno();
                bander = 0;
                regisAlu = newbandera(bander);
            }
	else{
		updateAlumno();}
	return false;
});

$('#btnDelete').click(function() {
	deleteAlumno();
	return false;
});

$('#wineList a').live('click', function() {
	findById($(this).data('identity'));
});

// Replace broken images with generic wine bottle
/*$("img").error(function(){
  $(this).attr("src", "pics/generic.jpg");

});*/

function search(searchKey) {
	if (searchKey == '') 
		findAll();
	else //if ( findByName(searchKey) === "^[A-Za-zÑñÁáÉéÍíÓóÚúÜü\s]+$"){
            findByName(searchKey);
            console.log("busqueda por nombres");
           /* findById(searchKey);
            console.log("busqueda por matricula");
            */
        /*}else{
            findById(searchKey);
        }*/
		
}

function newAlumno() {
	$('#btnDelete').hide();
	currentAlumno = {};
	renderDetails(currentAlumno); // Display empty form
}

function newbandera(bandera) {
        var band = bandera
	//activamos bandera
        if( band === 1 ){
            return 1;
        }
        else{
            return 0;
        }
}

function findAll() {
	console.log('imprime toda la lista');
	$.ajax({
		type: 'GET',
		url: rootURL,
		dataType: "json", // data type of response
		success: renderList
	});
}

function findByName(searchKey) {
	console.log('Encontrado por nombre: ' + searchKey);
	$.ajax({
		type: 'GET',
		url: rootURL + '/search/' + searchKey,
		dataType: "json",
		success: renderList 
	});
}

function findById(id) {
	console.log('Encontrado por matricula: ' + id);
	$.ajax({
		type: 'GET',
		url: rootURL + '/' + id,
		dataType: "json",
		success: function(data){
			$('#btnDelete').show();
			console.log('Encontrado por matricula exitosamente: ' + data.nombre);
			currentAlumno = data;
			renderDetails(currentAlumno);
		}
	});
}

function addAlumno() {
	console.log('Agregamos alumno');
	$.ajax({
		type: 'POST',
		contentType: 'application/json',
		url: rootURL,
		dataType: "json",
		data: formToJSON(),
		success: function(data, textStatus, jqXHR){
			alert('Alumno creado exitosamente');
			$('#btnDelete').show();
			$('#matricula').val(data.matricula);
		},
		error: function(jqXHR, textStatus, errorThrown){
			alert('Error al agregar Alumno: ' + textStatus);
		}
	});
}

function updateAlumno() {
	console.log('Actualizando Alumno');
	$.ajax({
		type: 'PUT',
		contentType: 'application/json',
		url: rootURL + '/' + $('#matricula').val(),
		dataType: "json",
		data: formToJSON(),
		success: function(data, textStatus, jqXHR){
			alert('Alumno actualizado exitosamente');
		},
		error: function(jqXHR, textStatus, errorThrown){
			alert('Error hay actualizar Alumno: ' + textStatus);
		}
	});
}

function deleteAlumno() {
	console.log('Borrar Alumno');
	$.ajax({
		type: 'DELETE',
		url: rootURL + '/' + $('#matricula').val(),
		success: function(data, textStatus, jqXHR){
			alert('Borrando Alumno exitosamente');
		},
		error: function(jqXHR, textStatus, errorThrown){
			alert('Error al borrar alumno');
		}
	});
}

function renderList(data) {
	// JAX-RS serializes an empty list as null, and a 'collection of one' as an object (not an 'array of one')
	var list = data == null ? [] : (data instanceof Array ? data : [data]);

	$('#wineList li').remove();
	$.each(list, function(index, alumno) {
		$('#wineList').append('<li><a href="#" data-identity="' + alumno.matricula + '">'+ alumno.nombre +'</a></li>');
	});
}

function renderDetails(alumno) {
	$('#matricula').val(alumno.matricula);
	$('#nombre').val(alumno.nombre);
	$('#edad').val(alumno.edad);
	$('#genero').val(alumno.genero);
	//$('#pic').attr('src', 'pics/' + wine.picture); //imagen
}

// Helper function to serialize all the form fields into a JSON string
function formToJSON() {
	return JSON.stringify({
		"matricula": $('#matricula').val(),
		"nombre": $('#nombre').val(), 
		"edad": $('#edad').val(),
		"genero": $('#genero').val()
		});
}
