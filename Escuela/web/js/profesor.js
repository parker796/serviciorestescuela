//ruta de nuestro servicio
var rootURL = "http://localhost:8080/Escuela/webresources/profesor";

var currentProfesor; //estas es una variable que va guardar los datos parseados del formulario a JSON
var bander = 0;
var regisAlu;
//imprimimos la lista 
findAll();

//ocutamos el boton de borrado
$('#btnDelete').hide();

// Register listeners
$('#btnSearch').click(function() {
	search($('#searchKey').val());
	return false;
});

// Trigger search when pressing 'Return' on search key input field
$('#searchKey').keypress(function(e){
	if(e.which == 13) {
		search($('#searchKey').val());
		e.preventDefault();
		return false;
    }
});

$('#btnAdd').click(function() {
	newProfesor();
        //creamos una bandera para detectar si se va registrar un alumno
        bander = 1;
        regisAlu = newbandera(bander);
	return false;
});

$('#btnSave').click(function() {
	if (regisAlu === 1){
		addProfesor();
                bander = 0;
                regisAlu = newbandera(bander);
            }
	else{
		updateProfesor();}
	return false;
});

$('#btnDelete').click(function() {
	deleteProfesor();
	return false;
});

$('#wineList a').live('click', function() {
	findById($(this).data('identity'));
});

// Replace broken images with generic wine bottle
/*$("img").error(function(){
  $(this).attr("src", "pics/generic.jpg");

});*/

function search(searchKey) {
	if (searchKey == '') 
		findAll();
	else
		findByName(searchKey);
}

function newProfesor() {
	$('#btnDelete').hide();
	currentProfesor = {};
	renderDetails(currentProfesor); // Display empty form
}

function newbandera(bandera) {
        var band = bandera
	//activamos bandera
        if( band === 1 ){
            return 1;
        }
        else{
            return 0;
        }
}

function findAll() {
	console.log('imprime toda la lista');
	$.ajax({
		type: 'GET',
		url: rootURL,
		dataType: "json", // data type of response
		success: renderList
	});
}

function findByName(searchKey) {
	console.log('Encontrado por nombre: ' + searchKey);
	$.ajax({
		type: 'GET',
		url: rootURL + '/search/' + searchKey,
		dataType: "json",
		success: renderList 
	});
}

function findById(id) {
	console.log('Encontrado por numero economico: ' + id);
	$.ajax({
		type: 'GET',
		url: rootURL + '/' + id,
		dataType: "json",
		success: function(data){
			$('#btnDelete').show();
			console.log('Encontrado por numero economico exitosamente: ' + data.nombre);
			currentProfesor = data;
			renderDetails(currentProfesor);
		}
	});
}

function addProfesor() {
	console.log('Agregamos Profesor');
	$.ajax({
		type: 'POST',
		contentType: 'application/json',
		url: rootURL,
		dataType: "json",
		data: formToJSON(),
		success: function(data, textStatus, jqXHR){
			alert('Profesor creado exitosamente');
			$('#btnDelete').show();
			$('#numEco').val(data.numEco);
		},
		error: function(jqXHR, textStatus, errorThrown){
			alert('Error al agregar Profesor: ' + textStatus);
		}
	});
}

function updateProfesor() {
	console.log('Actualizando Profesor');
	$.ajax({
		type: 'PUT',
		contentType: 'application/json',
		url: rootURL + '/' + $('#numEco').val(),
		dataType: "json",
		data: formToJSON(),
		success: function(data, textStatus, jqXHR){
			alert('Profesor actualizado exitosamente');
		},
		error: function(jqXHR, textStatus, errorThrown){
			alert('Error hay actualizar Profesor: ' + textStatus);
		}
	});
}

function deleteProfesor() {
	console.log('Borrar Profesor');
	$.ajax({
		type: 'DELETE',
		url: rootURL + '/' + $('#numEco').val(),
		success: function(data, textStatus, jqXHR){
			alert('Borrando Profesor exitosamente');
		},
		error: function(jqXHR, textStatus, errorThrown){
			alert('Error al borrar profesor');
		}
	});
}

function renderList(data) {
	// JAX-RS serializes an empty list as null, and a 'collection of one' as an object (not an 'array of one')
	var list = data == null ? [] : (data instanceof Array ? data : [data]);

	$('#wineList li').remove();
	$.each(list, function(index, profesor) {
		$('#wineList').append('<li><a href="#" data-identity="' + profesor.numEco + '">'+ profesor.nombre +'</a></li>');
	});
}

function renderDetails(profesor) {
	$('#numEco').val(profesor.numEco);
	$('#nombre').val(profesor.nombre);
	$('#grado').val(profesor.grado);
	//$('#pic').attr('src', 'pics/' + wine.picture); //imagen
}

// Helper function to serialize all the form fields into a JSON string
function formToJSON() {
	return JSON.stringify({
		"numEco": $('#numEco').val(),
		"nombre": $('#nombre').val(), 
		"grado": $('#grado').val()
		});
}



