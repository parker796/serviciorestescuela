//ruta de nuestro servicio
var rootURL = "http://localhost:8080/Escuela/webresources/horario";

var currentHorario; //estas es una variable que va guardar los datos parseados del formulario a JSON
var bander = 0;
var regisAlu;
//imprimimos la lista 
findAll();

//ocutamos el boton de borrado
$('#btnDelete').hide();

// Register listeners
$('#btnSearch').click(function () {
    search($('#searchKey').val());
    return false;
});

// Trigger search when pressing 'Return' on search key input field
$('#searchKey').keypress(function (e) {
    if (e.which == 13) {
        search($('#searchKey').val());
        e.preventDefault();
        return false;
    }
});

$('#btnAdd').click(function () {
    newHorario();
    //creamos una bandera para detectar si se va registrar un alumno
    bander = 1;
    regisAlu = newbandera(bander);
    return false;
});

$('#btnSave').click(function () {
    if (regisAlu === 1) {
        addHorario();
        bander = 0;
        regisAlu = newbandera(bander);
    } else {
        updateHorario();
    }
    return false;
});

$('#btnDelete').click(function () {
    deleteHorario();
    return false;
});

$('#wineList a').live('click', function () {
    findById($(this).data('identity'));
});

// Replace broken images with generic wine bottle
/*$("img").error(function(){
 $(this).attr("src", "pics/generic.jpg");
 
 });*/

function search(searchKey) {
    if (searchKey == '')
        findAll();
    else
        findByName(searchKey);
}

function newHorario() {
    $('#btnDelete').hide();
    currentHorario = {};
    renderDetails(currentHorario); // Display empty form
}

function newbandera(bandera) {
    var band = bandera
    //activamos bandera
    if (band === 1) {
        return 1;
    } else {
        return 0;
    }
}

function findAll() {
    console.log('imprime toda la lista');
    $.ajax({
        type: 'GET',
        url: rootURL,
        dataType: "json", // data type of response
        success: renderList
    });
}

function findByName(searchKey) {
    console.log('Encontrado por grupo: ' + searchKey);
    $.ajax({
        type: 'GET',
        url: rootURL + '/search/' + searchKey,
        dataType: "json",
        success: renderList
    });
}

function findById(id) {
    console.log('Encontrado por clave: ' + id);
    $.ajax({
        type: 'GET',
        url: rootURL + '/' + id,
        dataType: "json",
        success: function (data) {
            $('#btnDelete').show();
            console.log('Encontrado por clave exitosamente: ' + data.clave);
            currentHorario = data;
            renderDetails(currentHorario);
        }
    });
}

function addHorario() {
    console.log('Agregamos Horario');
    $.ajax({
        type: 'POST',
        contentType: 'application/json',
        url: rootURL,
        dataType: "json",
        data: formToJSON(),
        success: function (data, textStatus, jqXHR) {
            alert('Horario creado exitosamente');
            $('#btnDelete').show();
            $('#clave').val(data.clave);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Error al agregar Horario: ' + textStatus);
        }
    });
}

function updateHorario() {
    console.log('Actualizando Horario');
    $.ajax({
        type: 'PUT',
        contentType: 'application/json',
        url: rootURL + '/' + $('#clave').val(),
        dataType: "json",
        data: formToJSON(),
        success: function (data, textStatus, jqXHR) {
            alert('Horario actualizado exitosamente');
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Error hay actualizar Horario: ' + textStatus);
        }
    });
}

function deleteHorario() {
    console.log('Borrar Horario');
    $.ajax({
        type: 'DELETE',
        url: rootURL + '/' + $('#clave').val(),
        success: function (data, textStatus, jqXHR) {
            alert('Borrando horario exitosamente');
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Error al borrar horario');
        }
    });
}

function renderList(data) {
    // JAX-RS serializes an empty list as null, and a 'collection of one' as an object (not an 'array of one')
    var list = data == null ? [] : (data instanceof Array ? data : [data]);

    $('#wineList li').remove();
    $.each(list, function (index, horario) {
        $('#wineList').append('<li><a href="#" data-identity="' + horario.clave + '">' + horario.grupo + '</a></li>');
    });
}

function renderDetails(horario) {
    $('#clave').val(horario.clave),
    $('#grupo').val(horario.grupo);
    $('#salon').val(horario.salon);
    $('#num_eco').val(horario.num_eco);
    $('#clave_uea').val(horario.clave_uea);
    $('#hr_ini_l').val(horario.hr_ini_l);
    $('#hr_fin_l').val(horario.hr_fin_l);
    $('#hr_ini_m').val(horario.hr_ini_m);
    $('#hr_fin_m').val(horario.hr_fin_m);
    $('#hr_ini_z').val(horario.hr_ini_z);
    $('#hr_fin_z').val(horario.hr_fin_z);
    $('#hr_ini_j').val(horario.hr_ini_j);
    $('#hr_fin_j').val(horario.hr_fin_j);
    $('#hr_ini_v').val(horario.hr_ini_v);
    $('#hr_fin_v').val(horario.hr_fin_v);
    //$('#pic').attr('src', 'pics/' + wine.picture); //imagen
}

// Helper function to serialize all the form fields into a JSON string
function formToJSON() {
    return JSON.stringify({
        "clave": $('#clave').val(),
        "grupo": $('#grupo').val(),
        "salon": $('#salon').val(),
        "num_eco": $('#num_eco').val(),
        "clave_uea": $('#clave_uea').val(),
        "hr_ini_l": $('#hr_ini_l').val(),
        "hr_fin_l": $('#hr_fin_l').val(),
        "hr_ini_m": $('#hr_ini_m').val(),
        "hr_fin_m": $('#hr_fin_m').val(),
        "hr_ini_z": $('#hr_ini_z').val(),
        "hr_fin_z": $('#hr_fin_z').val(),
        "hr_ini_j": $('#hr_ini_j').val(),
        "hr_fin_j": $('#hr_fin_j').val(),
        "hr_ini_v": $('#hr_ini_v').val(),
        "hr_fin_v": $('#hr_fin_v').val()
    });
}






