/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paq;

import java.sql.Time;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement //esta anotacion sirve para que jax rs serialize un peticion en JSON
public class Horario {

    private int clave;
    private String grupo;

   
    private String salon;
    private int num_eco;
    private int clave_uea;

    private Time hr_fin_l;
    private Time hr_ini_l;

    private Time hr_ini_m; 
    private Time hr_fin_m ;

    private Time hr_ini_z; 
    private Time hr_fin_z; 

    private Time hr_ini_j; 
    private Time hr_fin_j; 

    private Time hr_ini_v;
    private Time hr_fin_v; 

   /* public Horario(int clave, String grupo, String salon, int num_eco, int clave_uea, Time hr_fin_l, Time hr_ini_l, Time hr_ini_m, Time hr_fin_m, Time hr_ini_z, Time hr_fin_z, Time hr_ini_j, Time hr_fin_j, Time hr_ini_v, Time hr_fin_v) {
        this.clave = clave;
        this.grupo = grupo;
        this.salon = salon;
        this.num_eco = num_eco;
        this.clave_uea = clave_uea;
        this.hr_fin_l = hr_fin_l;
        this.hr_ini_l = hr_ini_l;
        this.hr_ini_m = hr_ini_m;
        this.hr_fin_m = hr_fin_m;
        this.hr_ini_z = hr_ini_z;
        this.hr_fin_z = hr_fin_z;
        this.hr_ini_j = hr_ini_j;
        this.hr_fin_j = hr_fin_j;
        this.hr_ini_v = hr_ini_v;
        this.hr_fin_v = hr_fin_v;
    }
*/
    public void setClave(int clave) {
        this.clave = clave;
    }

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }

    public void setSalon(String salon) {
        this.salon = salon;
    }

    public void setNum_eco(int num_eco) {
        this.num_eco = num_eco;
    }

    public void setClave_uea(int clave_uea) {
        this.clave_uea = clave_uea;
    }

    public void setHr_fin_l(Time hr_fin_l) {
        this.hr_fin_l = hr_fin_l;
    }

    public void setHr_ini_l(Time hr_ini_l) {
        this.hr_ini_l = hr_ini_l;
    }

    public void setHr_ini_m(Time hr_ini_m) {
        this.hr_ini_m = hr_ini_m;
    }

    public void setHr_fin_m(Time hr_fin_m) {
        this.hr_fin_m = hr_fin_m;
    }

    public void setHr_ini_z(Time hr_ini_z) {
        this.hr_ini_z = hr_ini_z;
    }

    public void setHr_fin_z(Time hr_fin_z) {
        this.hr_fin_z = hr_fin_z;
    }

    public void setHr_ini_j(Time hr_ini_j) {
        this.hr_ini_j = hr_ini_j;
    }

    public void setHr_fin_j(Time hr_fin_j) {
        this.hr_fin_j = hr_fin_j;
    }

    public void setHr_ini_v(Time hr_ini_v) {
        this.hr_ini_v = hr_ini_v;
    }

    public void setHr_fin_v(Time hr_fin_v) {
        this.hr_fin_v = hr_fin_v;
    }

    public int getClave() {
        return clave;
    }

    public String getGrupo() {
        return grupo;
    }

    public String getSalon() {
        return salon;
    }

    public int getNum_eco() {
        return num_eco;
    }

    public int getClave_uea() {
        return clave_uea;
    }

    public Time getHr_fin_l() {
        return hr_fin_l;
    }

    public Time getHr_ini_l() {
        return hr_ini_l;
    }

    public Time getHr_ini_m() {
        return hr_ini_m;
    }

    public Time getHr_fin_m() {
        return hr_fin_m;
    }

    public Time getHr_ini_z() {
        return hr_ini_z;
    }

    public Time getHr_fin_z() {
        return hr_fin_z;
    }

    public Time getHr_ini_j() {
        return hr_ini_j;
    }

    public Time getHr_fin_j() {
        return hr_fin_j;
    }

    public Time getHr_ini_v() {
        return hr_ini_v;
    }

    public Time getHr_fin_v() {
        return hr_fin_v;
    }
    
     @Override
    public String toString() {
        return "Horario{" + "clave=" + clave + ", grupo=" + grupo + ", salon=" + salon + ", num_eco=" + num_eco + ", clave_uea=" + clave_uea + ", hr_fin_l=" + hr_fin_l + ", hr_ini_l=" + hr_ini_l + ", hr_ini_m=" + hr_ini_m + ", hr_fin_m=" + hr_fin_m + ", hr_ini_z=" + hr_ini_z + ", hr_fin_z=" + hr_fin_z + ", hr_ini_j=" + hr_ini_j + ", hr_fin_j=" + hr_fin_j + ", hr_ini_v=" + hr_ini_v + ", hr_fin_v=" + hr_fin_v + '}';
    }
    
}
