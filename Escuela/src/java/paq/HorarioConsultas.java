/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paq;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class HorarioConsultas {

    public List<Horario> findAll() {
        List<Horario> list = new ArrayList<>();
        Connection c = null;
        String sql = "SELECT * FROM horarios ORDER BY grupo";
        try {
            c = ConnectionHelper.getConnection();
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(sql);
            while (rs.next()) {
                list.add(processRow(rs));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            ConnectionHelper.close(c);
        }
        return list;
    }

    public List<Horario> findByName(String grupo) {
        List<Horario> list = new ArrayList<>();
        Connection c = null;
        String sql = "SELECT * FROM horarios as e "
                + "WHERE UPPER(grupo) LIKE ? "
                + "ORDER BY grupo";
        try {
            c = ConnectionHelper.getConnection();
            PreparedStatement ps = c.prepareStatement(sql);
            ps.setString(1, "%" + grupo.toUpperCase() + "%");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(processRow(rs));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            ConnectionHelper.close(c);
        }
        return list;
    }

    public Horario findById(int clave) {
        String sql = "SELECT * FROM horarios WHERE clave = ?";
        Horario horario = null;
        Connection c = null;
        try {
            c = ConnectionHelper.getConnection();
            PreparedStatement ps = c.prepareStatement(sql);
            ps.setInt(1, clave);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                horario = processRow(rs);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            ConnectionHelper.close(c);
        }
        return horario;
    }

    public Horario save(Horario horario) {
        return horario.getClave() > 0 ? update(horario) : create(horario); // id > 0 o edad
    }

    public Horario create(Horario horario) {
        Connection c = null;
        PreparedStatement ps = null;
        try {
            c = ConnectionHelper.getConnection();
            ps = c.prepareStatement("INSERT INTO horarios ( clave, grupo, salon, num_eco, clave_uea, hr_ini_l, hr_fin_l, hr_ini_m, hr_fin_m, hr_ini_z,"
                    + "hr_fin_z, hr_ini_j, hr_fin_j, hr_ini_v, hr_fin_v ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            ps.setInt(1, horario.getClave());
            ps.setString(2, horario.getGrupo());
            ps.setString(3, horario.getSalon());
            ps.setInt(4, horario.getNum_eco());
            ps.setInt(5, horario.getClave_uea());
            ps.setTime(6, horario.getHr_ini_l());
            ps.setTime(7, horario.getHr_fin_l());
            ps.setTime(8, horario.getHr_ini_m());
            ps.setTime(9, horario.getHr_fin_m());
            ps.setTime(10, horario.getHr_ini_z());
            ps.setTime(11, horario.getHr_fin_z());
            ps.setTime(12, horario.getHr_ini_j());
            ps.setTime(13, horario.getHr_fin_j());
            ps.setTime(14, horario.getHr_ini_v());
            ps.setTime(15, horario.getHr_fin_v());

            ps.executeUpdate();
            /* ResultSet rs = ps.getGeneratedKeys();
            rs.next();
            // Update the id in the returned object. This is important as this value must be returned to the client.
            int matricula = rs.getInt(1);
            alumno.setMatricula(String.valueOf(matricula));*/
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            ConnectionHelper.close(c);
        }
        return horario;
    }

    public Horario update(Horario horario) {
        Connection c = null;
        try {
            c = ConnectionHelper.getConnection();
            PreparedStatement ps = c.prepareStatement("UPDATE horarios SET grupo=?, salon=?, num_eco=?, clave_uea=?, hr_ini_l=?, hr_fin_l=?, hr_ini_m=?, hr_fin_m=?, hr_ini_z=?,"
                    + "hr_fin_z=?, hr_ini_j=?, hr_fin_j=?, hr_ini_v=?, hr_fin_v=? WHERE clave=?");
            //solo se actualizan los datos no la matricula
            ps.setString(1, horario.getGrupo());
            ps.setString(2, horario.getSalon());
            ps.setInt(3, horario.getNum_eco());
            ps.setInt(4, horario.getClave_uea());
            ps.setTime(5, horario.getHr_ini_l());
            ps.setTime(6, horario.getHr_fin_l());
            ps.setTime(7, horario.getHr_ini_m());
            ps.setTime(8, horario.getHr_fin_m());
            ps.setTime(9, horario.getHr_ini_z());
            ps.setTime(10, horario.getHr_fin_z());
            ps.setTime(11, horario.getHr_ini_j());
            ps.setTime(12, horario.getHr_fin_j());
            ps.setTime(13, horario.getHr_ini_v());
            ps.setTime(14, horario.getHr_fin_v());
            ps.setInt(15, horario.getClave());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            ConnectionHelper.close(c);
        }
        return horario;
    }

    public boolean remove(int clave) {
        Connection c = null;
        try {
            c = ConnectionHelper.getConnection();
            PreparedStatement ps = c.prepareStatement("DELETE FROM horarios WHERE clave=?");
            ps.setInt(1, clave);
            int count = ps.executeUpdate();
            return count == 1;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            ConnectionHelper.close(c);
        }
    }
//esto es de la parte de base de datos

    protected Horario processRow(ResultSet rs) throws SQLException {
        Horario horario = new Horario();
        horario.setClave(rs.getInt("clave"));
        horario.setGrupo(rs.getString("grupo"));
        horario.setSalon(rs.getString("salon"));
        horario.setNum_eco(rs.getInt("num_eco"));
        horario.setClave_uea(rs.getInt("clave_uea"));
        horario.setHr_ini_l(rs.getTime("hr_ini_l"));
        horario.setHr_fin_l(rs.getTime("hr_fin_l"));
        horario.setHr_ini_m(rs.getTime("hr_ini_m"));
        horario.setHr_fin_m(rs.getTime("hr_fin_m"));
        horario.setHr_ini_z(rs.getTime("hr_ini_z"));
        horario.setHr_fin_z(rs.getTime("hr_fin_z"));
        horario.setHr_ini_j(rs.getTime("hr_ini_j"));
        horario.setHr_fin_j(rs.getTime("hr_fin_j"));
        horario.setHr_ini_v(rs.getTime("hr_ini_v"));
        horario.setHr_fin_v(rs.getTime("hr_fin_v"));

        return horario;
    }
}
