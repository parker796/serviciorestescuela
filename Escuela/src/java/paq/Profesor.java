
package paq;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement //esta anotacion sirve para que jax rs serialize un peticion en JSON
public class Profesor {
    private String numEco;
    private String nombre;
    private String grado;

   /* public Profesor(String numEco, String nombre, String grado) {
        this.numEco = numEco;
        this.nombre = nombre;
        this.grado = grado;
    }*/

    public String getNumEco() {
        return numEco;
    }

    public void setNumEco(String numEco) {
        this.numEco = numEco;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getGrado() {
        return grado;
    }

    public void setGrado(String grado) {
        this.grado = grado;
    }

    @Override
    public String toString() {
        return "Profesor{" + "numEco=" + numEco + ", nombre=" + nombre + ", grado=" + grado + '}';
    }
    
    
}

