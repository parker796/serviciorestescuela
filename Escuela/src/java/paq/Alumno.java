
package paq;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement //esta anotacion sirve para que jax rs serialize un peticion en JSON
public class Alumno 
{
    private String matricula;
    private String nombre;
    private int edad;
    private String genero;

   /* public Alumno(String matricula, String nombre, int edad, String genero) {
        this.matricula = matricula;
        this.nombre = nombre;
        this.edad = edad;
        this.genero = genero;
    }*/

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    @Override
    public String toString() {
        return "Alumno{\n" + "matricula=" + matricula + ", nombre=" + nombre + ", edad=" + edad + ", genero=" + genero + '}';
    }
    
    
}

