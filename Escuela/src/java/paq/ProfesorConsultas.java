/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paq;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


public class ProfesorConsultas {
    public List<Profesor> findAll() {
        List<Profesor> list = new ArrayList<>();
        Connection c = null;
    	String sql = "SELECT * FROM profesor ORDER BY nombre";
        try {
            c = ConnectionHelper.getConnection();
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(sql);
            while (rs.next()) {
                list.add(processRow(rs));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
		} finally {
			ConnectionHelper.close(c);
		}
        return list;
    }

    
    public List<Profesor> findByName(String nombre) {
        List<Profesor> list = new ArrayList<>();
        Connection c = null;
    	String sql = "SELECT * FROM profesor as e " +
			"WHERE UPPER(nombre) LIKE ? " +	
			"ORDER BY nombre";
        try {
            c = ConnectionHelper.getConnection();
            PreparedStatement ps = c.prepareStatement(sql);
            ps.setString(1, "%" + nombre.toUpperCase() + "%");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(processRow(rs));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
		} finally {
			ConnectionHelper.close(c);
		}
        return list;
    }
    
    public Profesor findById(String numeco) {
    	String sql = "SELECT * FROM profesor WHERE num_econ = ?";
        Profesor profesor = null;
        Connection c = null;
        try {
            c = ConnectionHelper.getConnection();
            PreparedStatement ps = c.prepareStatement(sql);
            ps.setString(1, numeco);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                profesor = processRow(rs);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
		} finally {
			ConnectionHelper.close(c);
		}
        return profesor;
    }

    public Profesor save(Profesor profesor)
	{
		return profesor.getNumEco() != null ? update(profesor) : create(profesor); // id > 0 o edad
	}    
    
    public Profesor create(Profesor profesor) {
        Connection c = null;
        PreparedStatement ps = null;
        try {
            c = ConnectionHelper.getConnection();
            ps = c.prepareStatement("INSERT INTO profesor (num_econ, nombre, grado) VALUES (?, ?, ?)");
            ps.setString(1, profesor.getNumEco());
            ps.setString(2, profesor.getNombre());
            ps.setString(3, profesor.getGrado());
           
            
            ps.executeUpdate();
           /* ResultSet rs = ps.getGeneratedKeys();
            rs.next();
            // Update the id in the returned object. This is important as this value must be returned to the client.
            int matricula = rs.getInt(1);
            alumno.setMatricula(String.valueOf(matricula));*/
        } catch (SQLException e) {
            throw new RuntimeException(e);
		} finally {
			ConnectionHelper.close(c);
		}
        return profesor;
    }

    public Profesor update(Profesor profesor) {
        Connection c = null;
        try {
            c = ConnectionHelper.getConnection();
            PreparedStatement ps = c.prepareStatement("UPDATE profesor SET nombre=?, grado=? WHERE num_econ=?");
            //solo se actualizan los datos no el no economico
            ps.setString(1, profesor.getNombre());
            ps.setString(2, profesor.getGrado());
            ps.setString(3, profesor.getNumEco());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
		} finally {
			ConnectionHelper.close(c);
		}
        return profesor;
    }

    public boolean remove(String numeco) {
        Connection c = null;
        try {
            c = ConnectionHelper.getConnection();
            PreparedStatement ps = c.prepareStatement("DELETE FROM profesor WHERE num_econ=?");
            ps.setString(1, numeco);
            int count = ps.executeUpdate();
            return count == 1;
        } catch (SQLException e) {
            throw new RuntimeException(e);
		} finally {
			ConnectionHelper.close(c);
		}
    }
//esto es de la parte de base de datos
    protected Profesor processRow(ResultSet rs) throws SQLException {
        Profesor profesor = new Profesor();
        profesor.setNumEco(rs.getString("num_econ"));
        profesor.setNombre(rs.getString("nombre"));
        profesor.setGrado(rs.getString("grado"));
        
        
        return profesor;
    }
}
