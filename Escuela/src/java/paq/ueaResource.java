package paq;

import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("uea")
public class ueaResource {

    UeaConsultas ueasConsult = new UeaConsultas();

    @GET
    @Produces({MediaType.APPLICATION_JSON})//MediaType.APPLICATION_XML //podemos mketerle igual xml
    public List<uea> findAll() {
        System.out.println("todas las ueas");
        return ueasConsult.findAll();
    }

    @GET
    @Path("search/{query}")
    @Produces({MediaType.APPLICATION_JSON})
    public List<uea> findByName(@PathParam("query") String query) {
        System.out.println("findByName: " + query);
        return ueasConsult.findByName(query);
    }

    @GET
    @Path("{clave_uea}")
    @Produces({MediaType.APPLICATION_JSON})
    public uea findById(@PathParam("clave_uea") int clave_uea) {
        System.out.println("findById " + clave_uea);
        return ueasConsult.findById(clave_uea);
    }

    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public uea create(uea ueas) {
        System.out.println("crear uea");
        return ueasConsult.create(ueas);
    }

    @PUT
    @Path("{clave_uea}")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public uea update(uea ueas) {
        System.out.println("Actualiza uea: " + ueas.getNombre_uea());
        ueasConsult.update(ueas);
        return ueas;
    }

    @DELETE
    @Path("{clave_uea}")
    @Produces({MediaType.APPLICATION_JSON})
    public void remove(@PathParam("clave_uea") int clave_uea) {
        ueasConsult.remove(clave_uea);
    }

    
}