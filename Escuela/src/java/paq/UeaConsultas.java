/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paq;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class UeaConsultas {
    public List<uea> findAll() {
        List<uea> list = new ArrayList<>();
        Connection c = null;
    	String sql = "SELECT * FROM uea ORDER BY nombre_uea";
        try {
            c = ConnectionHelper.getConnection();
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(sql);
            while (rs.next()) {
                list.add(processRow(rs));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
		} finally {
			ConnectionHelper.close(c);
		}
        return list;
    }

    
    public List<uea> findByName(String nombre_uea) {
        List<uea> list = new ArrayList<>();
        Connection c = null;
    	String sql = "SELECT * FROM uea as e " +
			"WHERE UPPER(nombre_uea) LIKE ? " +	
			"ORDER BY nombre_uea";
        try {
            c = ConnectionHelper.getConnection();
            PreparedStatement ps = c.prepareStatement(sql);
            ps.setString(1, "%" + nombre_uea.toUpperCase() + "%");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(processRow(rs));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
		} finally {
			ConnectionHelper.close(c);
		}
        return list;
    }
    
    public uea findById(int clave_uea) {
    	String sql = "SELECT * FROM uea WHERE clave_uea = ?";
        uea ueas = null;
        Connection c = null;
        try {
            c = ConnectionHelper.getConnection();
            PreparedStatement ps = c.prepareStatement(sql);
            ps.setInt(1, clave_uea);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                ueas = processRow(rs);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
		} finally {
			ConnectionHelper.close(c);
		}
        return ueas;
    }

    public uea save(uea ueas)
	{
		return ueas.getClave_uea() > 0 ? update(ueas) : create(ueas); // id > 0 o edad
	}    
    
    public uea create(uea ueas) {
        Connection c = null;
        PreparedStatement ps = null;
        try {
            c = ConnectionHelper.getConnection();
            ps = c.prepareStatement("INSERT INTO uea (clave_uea, nombre_uea, creditos) VALUES (?, ?, ?)");
            ps.setInt(1, ueas.getClave_uea());
            ps.setString(2, ueas.getNombre_uea());
            ps.setInt(3, ueas.getCreditos());
           
            
            ps.executeUpdate();
           /* ResultSet rs = ps.getGeneratedKeys();
            rs.next();
            // Update the id in the returned object. This is important as this value must be returned to the client.
            int matricula = rs.getInt(1);
            alumno.setMatricula(String.valueOf(matricula));*/
        } catch (SQLException e) {
            throw new RuntimeException(e);
		} finally {
			ConnectionHelper.close(c);
		}
        return ueas;
    }

    public uea update(uea ueas) {
        Connection c = null;
        try {
            c = ConnectionHelper.getConnection();
            PreparedStatement ps = c.prepareStatement("UPDATE uea SET nombre_uea=?, creditos=? WHERE clave_uea=?");
            //solo se actualizan los datos no el no economico
            ps.setString(1, ueas.getNombre_uea());
            ps.setInt(2, ueas.getCreditos());
            ps.setInt(3, ueas.getClave_uea());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
		} finally {
			ConnectionHelper.close(c);
		}
        return ueas;
    }

    public boolean remove(int clave_uea) {
        Connection c = null;
        try {
            c = ConnectionHelper.getConnection();
            PreparedStatement ps = c.prepareStatement("DELETE FROM uea WHERE clave_uea=?");
            ps.setInt(1, clave_uea);
            int count = ps.executeUpdate();
            return count == 1;
        } catch (SQLException e) {
            throw new RuntimeException(e);
		} finally {
			ConnectionHelper.close(c);
		}
    }
//esto es de la parte de base de datos
    protected uea processRow(ResultSet rs) throws SQLException {
        uea ueas = new uea();
        ueas.setClave_uea(rs.getInt("clave_uea"));
        ueas.setNombre_uea(rs.getString("nombre_uea"));
        ueas.setCreditos(rs.getInt("creditos"));
       
        return ueas;
    }
}
