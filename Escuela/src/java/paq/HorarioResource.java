/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paq;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("horario")
public class HorarioResource {

    HorarioConsultas horarioConsult = new HorarioConsultas();

    @GET
    @Produces({MediaType.APPLICATION_JSON})//MediaType.APPLICATION_XML //podemos mketerle igual xml
    public List<Horario> findAll() {
        System.out.println("todos los horarios");
        return horarioConsult.findAll();
    }

    @GET
    @Path("search/{query}")
    @Produces({MediaType.APPLICATION_JSON})
    public List<Horario> findByName(@PathParam("query") String query) {
        System.out.println("findByName: " + query);
        return horarioConsult.findByName(query);
    }

    @GET
    @Path("{clave}")
    @Produces({MediaType.APPLICATION_JSON})
    public Horario findById(@PathParam("clave") int clave) {
        System.out.println("findById " + clave);
        return horarioConsult.findById(clave);
    }

    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Horario create(Horario horario) {
        System.out.println("crear alumno");
        return horarioConsult.create(horario);
    }

    @PUT
    @Path("{clave}")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Horario update(Horario horario) {
        System.out.println("Actualiza horario: " + horario.getGrupo());
        horarioConsult.update(horario);
        return horario;
    }

    @DELETE
    @Path("{clave}")
    @Produces({MediaType.APPLICATION_JSON})
    public void remove(@PathParam("clave") int clave) {
        horarioConsult.remove(clave);
    }

}
