
package paq;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement //esta anotacion sirve para que jax rs serialize un peticion en JSON
public class uea {

    private int clave_uea;
    private String nombre_uea;
    private int creditos;

   /* public uea(int clave_uea, String nombre_uea, int creditos) {
        this.clave_uea = clave_uea;
        this.nombre_uea = nombre_uea;
        this.creditos = creditos;
    }*/

    public void setClave_uea(int clave_uea) {
        this.clave_uea = clave_uea;
    }

    public void setNombre_uea(String nombre_uea) {
        this.nombre_uea = nombre_uea;
    }

    public void setCreditos(int creditos) {
        this.creditos = creditos;
    }

    public int getClave_uea() {
        return clave_uea;
    }

    public String getNombre_uea() {
        return nombre_uea;
    }

    public int getCreditos() {
        return creditos;
    }

     @Override
    public String toString() {
        return "uea{" + "clave_uea=" + clave_uea + ", nombre_uea=" + nombre_uea + ", creditos=" + creditos + '}';
    }
    
}
