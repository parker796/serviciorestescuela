/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paq;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


public class AlumnosConsultas {
    public List<Alumno> findAll() {
        List<Alumno> list = new ArrayList<>();
        Connection c = null;
    	String sql = "SELECT * FROM alumnos ORDER BY nombre";
        try {
            c = ConnectionHelper.getConnection();
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(sql);
            while (rs.next()) {
                list.add(processRow(rs));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
		} finally {
			ConnectionHelper.close(c);
		}
        return list;
    }

    
    public List<Alumno> findByName(String nombre) {
        List<Alumno> list = new ArrayList<>();
        Connection c = null;
    	String sql = "SELECT * FROM alumnos as e " +
			"WHERE UPPER(nombre) LIKE ? " +	
			"ORDER BY nombre";
        try {
            c = ConnectionHelper.getConnection();
            PreparedStatement ps = c.prepareStatement(sql);
            ps.setString(1, "%" + nombre.toUpperCase() + "%");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(processRow(rs));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
		} finally {
			ConnectionHelper.close(c);
		}
        return list;
    }
    
    public Alumno findById(String matricula) {
    	String sql = "SELECT * FROM alumnos WHERE matricula = ?";
        Alumno alumno = null;
        Connection c = null;
        try {
            c = ConnectionHelper.getConnection();
            PreparedStatement ps = c.prepareStatement(sql);
            ps.setString(1, matricula);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                alumno = processRow(rs);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
		} finally {
			ConnectionHelper.close(c);
		}
        return alumno;
    }

    public Alumno save(Alumno alumno)
	{
		return alumno.getMatricula() != null ? update(alumno) : create(alumno); // id > 0 o edad
	}    
    
    public Alumno create(Alumno alumno) {
        Connection c = null;
        PreparedStatement ps = null;
        try {
            c = ConnectionHelper.getConnection();
            ps = c.prepareStatement("INSERT INTO alumnos (matricula, nombre, edad, genero) VALUES (?, ?, ?, ?)");
            ps.setString(1, alumno.getMatricula());
            ps.setString(2, alumno.getNombre());
            ps.setInt(3, alumno.getEdad());
            ps.setString(4, alumno.getGenero());
            
            ps.executeUpdate();
           /* ResultSet rs = ps.getGeneratedKeys();
            rs.next();
            // Update the id in the returned object. This is important as this value must be returned to the client.
            int matricula = rs.getInt(1);
            alumno.setMatricula(String.valueOf(matricula));*/
        } catch (SQLException e) {
            throw new RuntimeException(e);
		} finally {
			ConnectionHelper.close(c);
		}
        return alumno;
    }

    public Alumno update(Alumno alumno) {
        Connection c = null;
        try {
            c = ConnectionHelper.getConnection();
            PreparedStatement ps = c.prepareStatement("UPDATE alumnos SET nombre=?, edad=?, genero=? WHERE matricula=?");
            //solo se actualizan los datos no la matricula
            ps.setString(1, alumno.getNombre());
            ps.setInt(2, alumno.getEdad());
            ps.setString(3, alumno.getGenero());
            ps.setString(4, alumno.getMatricula());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
		} finally {
			ConnectionHelper.close(c);
		}
        return alumno;
    }

    public boolean remove(String matricula) {
        Connection c = null;
        try {
            c = ConnectionHelper.getConnection();
            PreparedStatement ps = c.prepareStatement("DELETE FROM alumnos WHERE matricula=?");
            ps.setString(1, matricula);
            int count = ps.executeUpdate();
            return count == 1;
        } catch (SQLException e) {
            throw new RuntimeException(e);
		} finally {
			ConnectionHelper.close(c);
		}
    }
//esto es de la parte de base de datos
    protected Alumno processRow(ResultSet rs) throws SQLException {
        Alumno alumno = new Alumno();
        alumno.setMatricula(rs.getString("matricula"));
        alumno.setNombre(rs.getString("nombre"));
        alumno.setEdad(rs.getInt("edad"));
        alumno.setGenero(rs.getString("genero"));
        
        return alumno;
    }
}
