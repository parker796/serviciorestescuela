package paq;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


@Path("alumno")
public class AlumnoResource {
    
    AlumnosConsultas alumnoConsult = new AlumnosConsultas();

    @GET
	@Produces({MediaType.APPLICATION_JSON})//MediaType.APPLICATION_XML //podemos mketerle igual xml
	public List<Alumno> findAll() {
		System.out.println("todos los alumnos");
		return alumnoConsult.findAll();
	}
        
        @GET @Path("search/{query}")
	@Produces({ MediaType.APPLICATION_JSON})
	public List<Alumno> findByName(@PathParam("query") String query) {
		System.out.println("findByName: " + query);
		return alumnoConsult.findByName(query);
	}
        
        @GET @Path("{matricula}")
	@Produces({ MediaType.APPLICATION_JSON})
	public Alumno findById(@PathParam("matricula") String matricula) {
		System.out.println("findById " + matricula);
		return alumnoConsult.findById(matricula);
	}
        
        @POST
	@Consumes({ MediaType.APPLICATION_JSON})
	@Produces({ MediaType.APPLICATION_JSON})
	public Alumno create(Alumno alumno) {
		System.out.println("crear alumno");
		return alumnoConsult.create(alumno);
	}

	@PUT @Path("{matricula}")
	@Consumes({ MediaType.APPLICATION_JSON})
	@Produces({ MediaType.APPLICATION_JSON})
	public Alumno update(Alumno alumno) {
		System.out.println("Actualiza alumno: " + alumno.getNombre());
		alumnoConsult.update(alumno);
		return alumno;
	}
	
	@DELETE @Path("{matricula}")
	@Produces({ MediaType.APPLICATION_JSON})
	public void remove(@PathParam("matricula") String matricula) {
		alumnoConsult.remove(matricula);
	}
        
        
        
}