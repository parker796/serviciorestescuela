package paq;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("profesor")
public class ProfesorResource {

    ProfesorConsultas profesorConsult = new ProfesorConsultas();

    @GET
    @Produces({MediaType.APPLICATION_JSON})//MediaType.APPLICATION_XML //podemos mketerle igual xml
    public List<Profesor> findAll() {
        System.out.println("todos los profesores");
        return profesorConsult.findAll();
    }

    @GET
    @Path("search/{query}")
    @Produces({MediaType.APPLICATION_JSON})
    public List<Profesor> findByName(@PathParam("query") String query) {
        System.out.println("findByName: " + query);
        return profesorConsult.findByName(query);
    }

    @GET
    @Path("{numEco}")
    @Produces({MediaType.APPLICATION_JSON})
    public Profesor findById(@PathParam("numEco") String numeco) {
        System.out.println("findById " + numeco);
        return profesorConsult.findById(numeco);
    }

    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Profesor create(Profesor profesor) {
        System.out.println("crear profesor");
        return profesorConsult.create(profesor);
    }

    @PUT
    @Path("{numEco}")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Profesor update(Profesor profesor) {
        System.out.println("Actualiza profesor: " + profesor.getNombre());
        profesorConsult.update(profesor);
        return profesor;
    }

    @DELETE
    @Path("{numEco}")
    @Produces({MediaType.APPLICATION_JSON})
    public void remove(@PathParam("numEco") String numeco) {
        profesorConsult.remove(numeco);
    }

}
