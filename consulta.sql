USE agenda;
//CREAR TABLAS

CREATE TABLE alumnos(
matricula CHAR(9)NOT NULL,
nombre VARCHAR(50),
edad INT,
genero VARCHAR(20),
CONSTRAINT PRIMARY KEY(matricula))
ENGINE = InnoDB;

CREATE TABLE profesor(
num_econ int not NULL,
nombre varchar(70),
grado VARCHAR(20),
CONSTRAINT PRIMARY KEY (num_econ))
ENGINE = INNODB;

DROP TABLE profesor;

CREATE TABLE uea(
clave_uea int NOT NULL,
nombre_uea VARCHAR(70),
creditos INT NOT NULL, 
CONSTRAINT PRIMARY KEY (clave_uea))
 ENGINE = InnoDB;
 
DROP TABLE horarios;
CREATE TABLE horarios(
clave int auto_increment,
grupo VARCHAR(10) NOT NULL,
salon VARCHAR(10) NOT NULL,
num_eco INT NOT NULL,
CONSTRAINT  FOREIGN KEY (num_eco) REFERENCES profesor (num_econ),
clave_uea int not NULL, 
CONSTRAINT  FOREIGN KEY (clave_uea) REFERENCES uea (clave_uea),
hr_ini_l TIME,
hr_fin_l TIME,
hr_ini_m TIME,
hr_fin_m TIME,
hr_ini_z TIME,
hr_fin_z TIME,
hr_ini_j TIME,
hr_fin_j TIME,
hr_ini_v TIME,
hr_fin_v TIME,
constraint PRIMARY KEY (clave))
 ENGINE = InnoDB;
//LLENAR TABLAS
INSERT INTO alumnos VALUES("217303762","Maria Bravo",26,"Femenino");
INSERT INTO alumnos VALUES("217303092","Jerson De La Cruz",25,"Masculino");
INSERT INTO alumnos VALUES("217300000","Fabian Galicia",24,"Masculino");
INSERT INTO alumnos VALUES("217323145","Samanta Tapia",22,"Femenino");
INSERT INTO alumnos VALUES("217987654","Saul Mendoza",22,"Masculino");
INSERT INTO alumnos VALUES("213111112","Alan David Acevedo",22,"Masculino");
INSERT INTO alumnos VALUES("273015615","Adolfo Gonzalez",26,"Masculino");
INSERT INTO alumnos VALUES("173145235","Jennifer Delgado",21,"Femenino");
INSERT INTO alumnos VALUES("000000000","Aylin Solano",21,"Femenino");
INSERT INTO alumnos VALUES("874561132","Alejandro Servin",22,"Masculino");

INSERT INTO profesor VALUES(1111, "Alejandro Aguilar Zavoznik", "Doctor");
INSERT INTO profesor VALUES(5467, "Leonardo Daniel Sanchez Martinez", "Doctor");
INSERT INTO profesor VALUES(1234, "Maricela Claudia Bravo Contreras", "Doctora");
INSERT INTO profesor VALUES(2356, "Eduardo Basurto Uribe", "Doctor");
INSERT INTO profesor VALUES(987, "Rodrigo Castro Campos", "Doctor");
INSERT INTO profesor VALUES(9875, "Josue Figueroa Gonzalez", "Maestro");
INSERT INTO profesor VALUES(1928, "Juan Jesus Ocampo Hidalgo", "Doctor");
INSERT INTO profesor VALUES(5713, "Beatriz Adriana Gonzalez Beltran", "Doctora");
INSERT INTO profesor VALUES(1478, "Silvia Beatriz Gonzalez Brambila", "Doctora");
INSERT INTO profesor VALUES(7238, "Cutberto Romero Melendez", "Doctor");

INSERT INTO uea VALUES(1112034,"Lenguajes y Automatas",9);
INSERT INTO uea VALUES(1151047,"Bases de Datos",12);
INSERT INTO uea VALUES(1151042,"Algoritmos y Estructuras de Datos",8);
INSERT INTO uea VALUES(1151057,"Programación Orientada a Servicios",6);
INSERT INTO uea VALUES(1121043,"Diseño y Administracion de redes de computadoras",12);
INSERT INTO uea VALUES(1151048,"Analisis y diseno de sistemas de informacion",8);
INSERT INTO uea VALUES(1151044,"Programacion Orientada a Objetos",8);
INSERT INTO uea VALUES(1151076,"Programacion Visual Orientada a Eventos",7);

INSERT INTO uea VALUES(1151018,"Sistemas Operativos",9);
INSERT INTO uea VALUES(1151051,"Graficas por computadora",9);

/*AUTOMATAS*/
INSERT INTO horarios VALUES (1,"CCB01", "F300", 1111, 1112034, "16:00:00","17:30:00","00:00:00","00:00:00", "16:00:00","17:30:00","00:00:00","00:00:00","16:00:00","17:30:00");
/*ALGORITMOS*/
INSERT INTO horarios VALUES (2,"CSI01", "F302", 987, 1151042,"00:00:00","00:00:00","13:45:00","16:00:00","00:00:00","00:00:00","13:45:00","16:00:00","00:00:00","00:00:00");
/*POS*/
INSERT INTO horarios VALUES (3,"CSI81","F300", 1234, 1151057,"00:00:00","00:00:00","16:00:00","18:15:00","00:00:00","00:00:00","16:00:00","18:15:00","00:00:00","00:00:00");
/*POO*/
INSERT INTO horarios VALUES (4,"CSI01","F301",5467 ,1151044 ,"10:00:00","11:30:00","00:00:00","00:00:00","10:00:00","11:30:00","00:00:00","00:00:00","10:00:00","11:30:00");
INSERT INTO horarios VALUES (5,"CSI81", "F302",1234 , 1151044,"14:30:00","16:00:00","00:00:00","00:00:00","14:30:00","16:00:00","00:00:00","00:00:00","14:30:00","16:00:00");
INSERT INTO horarios VALUES (6,"LDIV01", "F303",1478 ,1151044 ,"00:00:00","00:00:00","11:30:00","13:00:00","00:00:00","00:00:00","11:30:00","13:00:00","00:00:00","00:00:00");
/*POE*/
INSERT INTO horarios VALUES (7,"CSI01", "F300", 9875,1151076 ,"10:00:00","11:30:00","00:00:00","00:00:00","10:00:00","11:30:00","00:00:00","00:00:00","10:00:00","11:30:00");
INSERT INTO horarios VALUES (8,"CSI81", "F301",7238 , 1151018,"13:00:00","14:30:00","00:00:00","00:00:00","13:00:00","14:30:00","00:00:00","00:00:00","13:00:00","14:30:00");

/*BASE DE DATOS*/
INSERT INTO horarios VALUES (9,"CSI01","F301",5467, 1151047,"11:30:00","13:00:00","11:30:00","13:00:00","11:30:00","13:00:00","11:30:00","13:00:00","11:30:00","13:00:00");
INSERT INTO horarios VALUES (10,"CSI02", "F303", 9875, 1151042,"11:30:00","13:00:00","00:00:00","00:00:00","11:30:00","13:00:00","00:00:00","00:00:00","11:30:00","13:00:00");

INSERT INTO horarios VALUES (11,"CSI02", "F303", 987, 1151042,"11:30:00","13:00:00","19:30:00","21:00:00","11:30:00","13:00:00","00:00:00","00:00:00","11:30:00","13:00:00");
//MOSTRAR TABLAS
SHOW TABLES;
SELECT * FROM alumnos;
SELECT * FROM horarios;
SELECT * FROM profesor;
SELECT * from uea;
DROP TABLE horarios;
DROP TABLE alumnos;
DROP TABLE profesor;
DROP TABLE uea;

/*En que salon se encuentra el profesor x ahora*/
SELECT p.nombre,h.salon,h.hr_ini_m, h.hr_fin_m FROM profesor p INNER JOIN horarios h ON p.num_econ = h.num_eco
WHERE ((select CURTIME()>= h.hr_ini_m) AND (SELECT CURTIME()<= h.hr_fin_m ) );

/*Que ueas se dan los martes en x salon*/
/*suponiendo que el salon es el f300*/
SELECT uea.nombre_uea FROM uea INNER JOIN horarios ON uea.clave_uea=horarios.clave_uea WHERE ((horarios.salon = "F300") AND (horarios.hr_ini_m != "00:00:00"));


/*suponiendo que el salon es el f301*/
SELECT uea.nombre_uea FROM uea INNER JOIN horarios ON uea.clave_uea=horarios.clave_uea WHERE ((horarios.salon = "F301") AND (horarios.hr_ini_m != "00:00:00"));

/*Horario de x profesor*/
SELECT p.nombre, h.salon, h.hr_ini_l, h.hr_fin_l,h.hr_ini_m, h.hr_fin_m,h.hr_ini_z, h.hr_fin_z,h.hr_ini_j, h.hr_fin_j,h.hr_ini_v, h.hr_fin_v
FROM profesor p INNER JOIN horarios h ON p.num_econ=h.num_eco WHERE p.num_econ=9875;

/*ueas con la palabra programacion*/
SELECT nombre_uea from uea WHERE( (nombre_uea LIKE '%programacion%') OR (nombre_uea LIKE '%Programacion%'));

/*ueas que se imparten martes y jueves de 7 pm a 8 pm*/
SELECT uea.nombre_uea FROM uea INNER JOIN horarios ON uea.clave_uea=horarios.clave_uea WHERE ((horarios.hr_ini_m>="19:00:00") AND (horarios.hr_fin_m <= "20:00:00"));

/*Creditos de x uea*/
SELECT uea.creditos FROM uea WHERE (clave_uea = 1151047);

/*nombre de profesores que imparten uea POO*/

SELECT profesor.nombre FROM profesor INNER JOIN horarios ON profesor.num_econ=horarios.num_eco WHERE
((SELECT horarios.clave_uea FROM uea INNER JOIN horarios ON uea.clave_uea = horarios.clave_uea WHERE uea.nombre_uea="Programacion orientada a objetos") );